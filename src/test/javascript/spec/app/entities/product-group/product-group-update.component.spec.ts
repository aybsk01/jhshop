import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { Jshop11TestModule } from '../../../test.module';
import { ProductGroupUpdateComponent } from 'app/entities/product-group/product-group-update.component';
import { ProductGroupService } from 'app/entities/product-group/product-group.service';
import { ProductGroup } from 'app/shared/model/product-group.model';

describe('Component Tests', () => {
  describe('ProductGroup Management Update Component', () => {
    let comp: ProductGroupUpdateComponent;
    let fixture: ComponentFixture<ProductGroupUpdateComponent>;
    let service: ProductGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Jshop11TestModule],
        declarations: [ProductGroupUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ProductGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProductGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
