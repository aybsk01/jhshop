import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { PurchaseService } from 'app/entities/purchase/purchase.service';
import { IPurchase, Purchase } from 'app/shared/model/purchase.model';
import { PurchaseStatus } from 'app/shared/model/enumerations/purchase-status.model';

describe('Service Tests', () => {
  describe('Purchase Service', () => {
    let injector: TestBed;
    let service: PurchaseService;
    let httpMock: HttpTestingController;
    let elemDefault: IPurchase;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(PurchaseService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Purchase(0, currentDate, PurchaseStatus.FINISHED, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dt: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Purchase', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dt: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dt: currentDate
          },
          returnedFromService
        );
        service
          .create(new Purchase(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Purchase', () => {
        const returnedFromService = Object.assign(
          {
            dt: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            docNumber: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dt: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Purchase', () => {
        const returnedFromService = Object.assign(
          {
            dt: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            docNumber: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dt: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Purchase', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
