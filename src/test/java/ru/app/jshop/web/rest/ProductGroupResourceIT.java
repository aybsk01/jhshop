package ru.app.jshop.web.rest;

import ru.app.jshop.Jshop11App;
import ru.app.jshop.domain.ProductGroup;
import ru.app.jshop.repository.ProductGroupRepository;
import ru.app.jshop.service.ProductGroupService;
import ru.app.jshop.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.app.jshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductGroupResource} REST controller.
 */
@SpringBootTest(classes = Jshop11App.class)
public class ProductGroupResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ProductGroupRepository productGroupRepository;

    @Autowired
    private ProductGroupService productGroupService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProductGroupMockMvc;

    private ProductGroup productGroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductGroupResource productGroupResource = new ProductGroupResource(productGroupService);
        this.restProductGroupMockMvc = MockMvcBuilders.standaloneSetup(productGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductGroup createEntity(EntityManager em) {
        ProductGroup productGroup = new ProductGroup()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return productGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductGroup createUpdatedEntity(EntityManager em) {
        ProductGroup productGroup = new ProductGroup()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return productGroup;
    }

    @BeforeEach
    public void initTest() {
        productGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductGroup() throws Exception {
        int databaseSizeBeforeCreate = productGroupRepository.findAll().size();

        // Create the ProductGroup
        restProductGroupMockMvc.perform(post("/api/product-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productGroup)))
            .andExpect(status().isCreated());

        // Validate the ProductGroup in the database
        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeCreate + 1);
        ProductGroup testProductGroup = productGroupList.get(productGroupList.size() - 1);
        assertThat(testProductGroup.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProductGroup.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createProductGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productGroupRepository.findAll().size();

        // Create the ProductGroup with an existing ID
        productGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductGroupMockMvc.perform(post("/api/product-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productGroup)))
            .andExpect(status().isBadRequest());

        // Validate the ProductGroup in the database
        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = productGroupRepository.findAll().size();
        // set the field null
        productGroup.setName(null);

        // Create the ProductGroup, which fails.

        restProductGroupMockMvc.perform(post("/api/product-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productGroup)))
            .andExpect(status().isBadRequest());

        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductGroups() throws Exception {
        // Initialize the database
        productGroupRepository.saveAndFlush(productGroup);

        // Get all the productGroupList
        restProductGroupMockMvc.perform(get("/api/product-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    @Transactional
    public void getProductGroup() throws Exception {
        // Initialize the database
        productGroupRepository.saveAndFlush(productGroup);

        // Get the productGroup
        restProductGroupMockMvc.perform(get("/api/product-groups/{id}", productGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(productGroup.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProductGroup() throws Exception {
        // Get the productGroup
        restProductGroupMockMvc.perform(get("/api/product-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductGroup() throws Exception {
        // Initialize the database
        productGroupService.save(productGroup);

        int databaseSizeBeforeUpdate = productGroupRepository.findAll().size();

        // Update the productGroup
        ProductGroup updatedProductGroup = productGroupRepository.findById(productGroup.getId()).get();
        // Disconnect from session so that the updates on updatedProductGroup are not directly saved in db
        em.detach(updatedProductGroup);
        updatedProductGroup
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restProductGroupMockMvc.perform(put("/api/product-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductGroup)))
            .andExpect(status().isOk());

        // Validate the ProductGroup in the database
        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeUpdate);
        ProductGroup testProductGroup = productGroupList.get(productGroupList.size() - 1);
        assertThat(testProductGroup.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductGroup.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingProductGroup() throws Exception {
        int databaseSizeBeforeUpdate = productGroupRepository.findAll().size();

        // Create the ProductGroup

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductGroupMockMvc.perform(put("/api/product-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(productGroup)))
            .andExpect(status().isBadRequest());

        // Validate the ProductGroup in the database
        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductGroup() throws Exception {
        // Initialize the database
        productGroupService.save(productGroup);

        int databaseSizeBeforeDelete = productGroupRepository.findAll().size();

        // Delete the productGroup
        restProductGroupMockMvc.perform(delete("/api/product-groups/{id}", productGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductGroup> productGroupList = productGroupRepository.findAll();
        assertThat(productGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductGroup.class);
        ProductGroup productGroup1 = new ProductGroup();
        productGroup1.setId(1L);
        ProductGroup productGroup2 = new ProductGroup();
        productGroup2.setId(productGroup1.getId());
        assertThat(productGroup1).isEqualTo(productGroup2);
        productGroup2.setId(2L);
        assertThat(productGroup1).isNotEqualTo(productGroup2);
        productGroup1.setId(null);
        assertThat(productGroup1).isNotEqualTo(productGroup2);
    }
}
