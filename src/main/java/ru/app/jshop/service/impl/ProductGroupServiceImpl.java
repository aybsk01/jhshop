package ru.app.jshop.service.impl;

import ru.app.jshop.service.ProductGroupService;
import ru.app.jshop.domain.ProductGroup;
import ru.app.jshop.repository.ProductGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductGroup}.
 */
@Service
@Transactional
public class ProductGroupServiceImpl implements ProductGroupService {

    private final Logger log = LoggerFactory.getLogger(ProductGroupServiceImpl.class);

    private final ProductGroupRepository productGroupRepository;

    public ProductGroupServiceImpl(ProductGroupRepository productGroupRepository) {
        this.productGroupRepository = productGroupRepository;
    }

    /**
     * Save a productGroup.
     *
     * @param productGroup the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProductGroup save(ProductGroup productGroup) {
        log.debug("Request to save ProductGroup : {}", productGroup);
        return productGroupRepository.save(productGroup);
    }

    /**
     * Get all the productGroups.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProductGroup> findAll() {
        log.debug("Request to get all ProductGroups");
        return productGroupRepository.findAll();
    }


    /**
     * Get one productGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProductGroup> findOne(Long id) {
        log.debug("Request to get ProductGroup : {}", id);
        return productGroupRepository.findById(id);
    }

    /**
     * Delete the productGroup by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductGroup : {}", id);
        productGroupRepository.deleteById(id);
    }
}
