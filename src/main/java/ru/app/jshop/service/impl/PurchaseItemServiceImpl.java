package ru.app.jshop.service.impl;

import ru.app.jshop.service.PurchaseItemService;
import ru.app.jshop.domain.PurchaseItem;
import ru.app.jshop.repository.PurchaseItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PurchaseItem}.
 */
@Service
@Transactional
public class PurchaseItemServiceImpl implements PurchaseItemService {

    private final Logger log = LoggerFactory.getLogger(PurchaseItemServiceImpl.class);

    private final PurchaseItemRepository purchaseItemRepository;

    public PurchaseItemServiceImpl(PurchaseItemRepository purchaseItemRepository) {
        this.purchaseItemRepository = purchaseItemRepository;
    }

    /**
     * Save a purchaseItem.
     *
     * @param purchaseItem the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PurchaseItem save(PurchaseItem purchaseItem) {
        log.debug("Request to save PurchaseItem : {}", purchaseItem);
        return purchaseItemRepository.save(purchaseItem);
    }

    /**
     * Get all the purchaseItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PurchaseItem> findAll(Pageable pageable) {
        log.debug("Request to get all PurchaseItems");
        return purchaseItemRepository.findAll(pageable);
    }


    /**
     * Get one purchaseItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PurchaseItem> findOne(Long id) {
        log.debug("Request to get PurchaseItem : {}", id);
        return purchaseItemRepository.findById(id);
    }

    /**
     * Delete the purchaseItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PurchaseItem : {}", id);
        purchaseItemRepository.deleteById(id);
    }
}
