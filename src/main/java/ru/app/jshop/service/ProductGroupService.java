package ru.app.jshop.service;

import ru.app.jshop.domain.ProductGroup;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ProductGroup}.
 */
public interface ProductGroupService {

    /**
     * Save a productGroup.
     *
     * @param productGroup the entity to save.
     * @return the persisted entity.
     */
    ProductGroup save(ProductGroup productGroup);

    /**
     * Get all the productGroups.
     *
     * @return the list of entities.
     */
    List<ProductGroup> findAll();


    /**
     * Get the "id" productGroup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductGroup> findOne(Long id);

    /**
     * Delete the "id" productGroup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
