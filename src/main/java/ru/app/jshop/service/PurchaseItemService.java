package ru.app.jshop.service;

import ru.app.jshop.domain.PurchaseItem;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link PurchaseItem}.
 */
public interface PurchaseItemService {

    /**
     * Save a purchaseItem.
     *
     * @param purchaseItem the entity to save.
     * @return the persisted entity.
     */
    PurchaseItem save(PurchaseItem purchaseItem);

    /**
     * Get all the purchaseItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PurchaseItem> findAll(Pageable pageable);


    /**
     * Get the "id" purchaseItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PurchaseItem> findOne(Long id);

    /**
     * Delete the "id" purchaseItem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
