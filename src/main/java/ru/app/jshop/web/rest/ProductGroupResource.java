package ru.app.jshop.web.rest;

import ru.app.jshop.domain.ProductGroup;
import ru.app.jshop.service.ProductGroupService;
import ru.app.jshop.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.app.jshop.domain.ProductGroup}.
 */
@RestController
@RequestMapping("/api")
public class ProductGroupResource {

    private final Logger log = LoggerFactory.getLogger(ProductGroupResource.class);

    private static final String ENTITY_NAME = "productGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductGroupService productGroupService;

    public ProductGroupResource(ProductGroupService productGroupService) {
        this.productGroupService = productGroupService;
    }

    /**
     * {@code POST  /product-groups} : Create a new productGroup.
     *
     * @param productGroup the productGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productGroup, or with status {@code 400 (Bad Request)} if the productGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-groups")
    public ResponseEntity<ProductGroup> createProductGroup(@Valid @RequestBody ProductGroup productGroup) throws URISyntaxException {
        log.debug("REST request to save ProductGroup : {}", productGroup);
        if (productGroup.getId() != null) {
            throw new BadRequestAlertException("A new productGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductGroup result = productGroupService.save(productGroup);
        return ResponseEntity.created(new URI("/api/product-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-groups} : Updates an existing productGroup.
     *
     * @param productGroup the productGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productGroup,
     * or with status {@code 400 (Bad Request)} if the productGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-groups")
    public ResponseEntity<ProductGroup> updateProductGroup(@Valid @RequestBody ProductGroup productGroup) throws URISyntaxException {
        log.debug("REST request to update ProductGroup : {}", productGroup);
        if (productGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductGroup result = productGroupService.save(productGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productGroup.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-groups} : get all the productGroups.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productGroups in body.
     */
    @GetMapping("/product-groups")
    public List<ProductGroup> getAllProductGroups() {
        log.debug("REST request to get all ProductGroups");
        return productGroupService.findAll();
    }

    /**
     * {@code GET  /product-groups/:id} : get the "id" productGroup.
     *
     * @param id the id of the productGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-groups/{id}")
    public ResponseEntity<ProductGroup> getProductGroup(@PathVariable Long id) {
        log.debug("REST request to get ProductGroup : {}", id);
        Optional<ProductGroup> productGroup = productGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productGroup);
    }

    /**
     * {@code DELETE  /product-groups/:id} : delete the "id" productGroup.
     *
     * @param id the id of the productGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-groups/{id}")
    public ResponseEntity<Void> deleteProductGroup(@PathVariable Long id) {
        log.debug("REST request to delete ProductGroup : {}", id);
        productGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
