/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.app.jshop.web.rest.vm;
