package ru.app.jshop.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE
}
