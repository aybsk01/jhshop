package ru.app.jshop.domain.enumeration;

/**
 * The PurchaseStatus enumeration.
 */
public enum PurchaseStatus {
    FINISHED, PENDING
}
