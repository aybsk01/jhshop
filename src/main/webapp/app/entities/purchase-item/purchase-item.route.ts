import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PurchaseItem } from 'app/shared/model/purchase-item.model';
import { PurchaseItemService } from './purchase-item.service';
import { PurchaseItemComponent } from './purchase-item.component';
import { PurchaseItemDetailComponent } from './purchase-item-detail.component';
import { PurchaseItemUpdateComponent } from './purchase-item-update.component';
import { PurchaseItemDeletePopupComponent } from './purchase-item-delete-dialog.component';
import { IPurchaseItem } from 'app/shared/model/purchase-item.model';

@Injectable({ providedIn: 'root' })
export class PurchaseItemResolve implements Resolve<IPurchaseItem> {
  constructor(private service: PurchaseItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPurchaseItem> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PurchaseItem>) => response.ok),
        map((purchaseItem: HttpResponse<PurchaseItem>) => purchaseItem.body)
      );
    }
    return of(new PurchaseItem());
  }
}

export const purchaseItemRoute: Routes = [
  {
    path: '',
    component: PurchaseItemComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.purchaseItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PurchaseItemDetailComponent,
    resolve: {
      purchaseItem: PurchaseItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.purchaseItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PurchaseItemUpdateComponent,
    resolve: {
      purchaseItem: PurchaseItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.purchaseItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PurchaseItemUpdateComponent,
    resolve: {
      purchaseItem: PurchaseItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.purchaseItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const purchaseItemPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PurchaseItemDeletePopupComponent,
    resolve: {
      purchaseItem: PurchaseItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.purchaseItem.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
