import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPurchaseItem, PurchaseItem } from 'app/shared/model/purchase-item.model';
import { PurchaseItemService } from './purchase-item.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { IPurchase } from 'app/shared/model/purchase.model';
import { PurchaseService } from 'app/entities/purchase/purchase.service';

@Component({
  selector: 'jhi-purchase-item-update',
  templateUrl: './purchase-item-update.component.html'
})
export class PurchaseItemUpdateComponent implements OnInit {
  isSaving: boolean;

  products: IProduct[];

  purchases: IPurchase[];

  editForm = this.fb.group({
    id: [],
    quantity: [null, [Validators.required, Validators.min(0)]],
    product: [null, Validators.required],
    purchase: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected purchaseItemService: PurchaseItemService,
    protected productService: ProductService,
    protected purchaseService: PurchaseService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ purchaseItem }) => {
      this.updateForm(purchaseItem);
    });
    this.productService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProduct[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProduct[]>) => response.body)
      )
      .subscribe((res: IProduct[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.purchaseService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPurchase[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPurchase[]>) => response.body)
      )
      .subscribe((res: IPurchase[]) => (this.purchases = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(purchaseItem: IPurchaseItem) {
    this.editForm.patchValue({
      id: purchaseItem.id,
      quantity: purchaseItem.quantity,
      product: purchaseItem.product,
      purchase: purchaseItem.purchase
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const purchaseItem = this.createFromForm();
    if (purchaseItem.id !== undefined) {
      this.subscribeToSaveResponse(this.purchaseItemService.update(purchaseItem));
    } else {
      this.subscribeToSaveResponse(this.purchaseItemService.create(purchaseItem));
    }
  }

  private createFromForm(): IPurchaseItem {
    return {
      ...new PurchaseItem(),
      id: this.editForm.get(['id']).value,
      quantity: this.editForm.get(['quantity']).value,
      product: this.editForm.get(['product']).value,
      purchase: this.editForm.get(['purchase']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPurchaseItem>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductById(index: number, item: IProduct) {
    return item.id;
  }

  trackPurchaseById(index: number, item: IPurchase) {
    return item.id;
  }
}
