import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Jshop11SharedModule } from 'app/shared/shared.module';
import { PurchaseItemComponent } from './purchase-item.component';
import { PurchaseItemDetailComponent } from './purchase-item-detail.component';
import { PurchaseItemUpdateComponent } from './purchase-item-update.component';
import { PurchaseItemDeletePopupComponent, PurchaseItemDeleteDialogComponent } from './purchase-item-delete-dialog.component';
import { purchaseItemRoute, purchaseItemPopupRoute } from './purchase-item.route';

const ENTITY_STATES = [...purchaseItemRoute, ...purchaseItemPopupRoute];

@NgModule({
  imports: [Jshop11SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PurchaseItemComponent,
    PurchaseItemDetailComponent,
    PurchaseItemUpdateComponent,
    PurchaseItemDeleteDialogComponent,
    PurchaseItemDeletePopupComponent
  ],
  entryComponents: [PurchaseItemDeleteDialogComponent]
})
export class Jshop11PurchaseItemModule {}
