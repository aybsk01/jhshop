import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPurchase, Purchase } from 'app/shared/model/purchase.model';
import { PurchaseService } from './purchase.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';

@Component({
  selector: 'jhi-purchase-update',
  templateUrl: './purchase-update.component.html'
})
export class PurchaseUpdateComponent implements OnInit {
  isSaving: boolean;

  clients: IClient[];

  editForm = this.fb.group({
    id: [],
    dt: [null, [Validators.required]],
    status: [null, [Validators.required]],
    docNumber: [null, [Validators.required]],
    client: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected purchaseService: PurchaseService,
    protected clientService: ClientService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ purchase }) => {
      this.updateForm(purchase);
    });
    this.clientService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IClient[]>) => mayBeOk.ok),
        map((response: HttpResponse<IClient[]>) => response.body)
      )
      .subscribe((res: IClient[]) => (this.clients = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(purchase: IPurchase) {
    this.editForm.patchValue({
      id: purchase.id,
      dt: purchase.dt != null ? purchase.dt.format(DATE_TIME_FORMAT) : null,
      status: purchase.status,
      docNumber: purchase.docNumber,
      client: purchase.client
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const purchase = this.createFromForm();
    if (purchase.id !== undefined) {
      this.subscribeToSaveResponse(this.purchaseService.update(purchase));
    } else {
      this.subscribeToSaveResponse(this.purchaseService.create(purchase));
    }
  }

  private createFromForm(): IPurchase {
    return {
      ...new Purchase(),
      id: this.editForm.get(['id']).value,
      dt: this.editForm.get(['dt']).value != null ? moment(this.editForm.get(['dt']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      docNumber: this.editForm.get(['docNumber']).value,
      client: this.editForm.get(['client']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPurchase>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackClientById(index: number, item: IClient) {
    return item.id;
  }
}
