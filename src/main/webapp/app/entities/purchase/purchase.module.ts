import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Jshop11SharedModule } from 'app/shared/shared.module';
import { PurchaseComponent } from './purchase.component';
import { PurchaseDetailComponent } from './purchase-detail.component';
import { PurchaseUpdateComponent } from './purchase-update.component';
import { PurchaseDeletePopupComponent, PurchaseDeleteDialogComponent } from './purchase-delete-dialog.component';
import { purchaseRoute, purchasePopupRoute } from './purchase.route';

const ENTITY_STATES = [...purchaseRoute, ...purchasePopupRoute];

@NgModule({
  imports: [Jshop11SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PurchaseComponent,
    PurchaseDetailComponent,
    PurchaseUpdateComponent,
    PurchaseDeleteDialogComponent,
    PurchaseDeletePopupComponent
  ],
  entryComponents: [PurchaseDeleteDialogComponent]
})
export class Jshop11PurchaseModule {}
