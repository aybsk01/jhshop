import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.Jshop11ProductModule)
      },
      {
        path: 'product-group',
        loadChildren: () => import('./product-group/product-group.module').then(m => m.Jshop11ProductGroupModule)
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.Jshop11ClientModule)
      },
      {
        path: 'purchase',
        loadChildren: () => import('./purchase/purchase.module').then(m => m.Jshop11PurchaseModule)
      },
      {
        path: 'purchase-item',
        loadChildren: () => import('./purchase-item/purchase-item.module').then(m => m.Jshop11PurchaseItemModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class Jshop11EntityModule {}
