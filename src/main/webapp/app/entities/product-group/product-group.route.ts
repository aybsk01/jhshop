import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ProductGroup } from 'app/shared/model/product-group.model';
import { ProductGroupService } from './product-group.service';
import { ProductGroupComponent } from './product-group.component';
import { ProductGroupDetailComponent } from './product-group-detail.component';
import { ProductGroupUpdateComponent } from './product-group-update.component';
import { ProductGroupDeletePopupComponent } from './product-group-delete-dialog.component';
import { IProductGroup } from 'app/shared/model/product-group.model';

@Injectable({ providedIn: 'root' })
export class ProductGroupResolve implements Resolve<IProductGroup> {
  constructor(private service: ProductGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IProductGroup> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ProductGroup>) => response.ok),
        map((productGroup: HttpResponse<ProductGroup>) => productGroup.body)
      );
    }
    return of(new ProductGroup());
  }
}

export const productGroupRoute: Routes = [
  {
    path: '',
    component: ProductGroupComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.productGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ProductGroupDetailComponent,
    resolve: {
      productGroup: ProductGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.productGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ProductGroupUpdateComponent,
    resolve: {
      productGroup: ProductGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.productGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ProductGroupUpdateComponent,
    resolve: {
      productGroup: ProductGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.productGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const productGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ProductGroupDeletePopupComponent,
    resolve: {
      productGroup: ProductGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jshop11App.productGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
