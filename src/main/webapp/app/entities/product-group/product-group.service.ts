import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductGroup } from 'app/shared/model/product-group.model';

type EntityResponseType = HttpResponse<IProductGroup>;
type EntityArrayResponseType = HttpResponse<IProductGroup[]>;

@Injectable({ providedIn: 'root' })
export class ProductGroupService {
  public resourceUrl = SERVER_API_URL + 'api/product-groups';

  constructor(protected http: HttpClient) {}

  create(productGroup: IProductGroup): Observable<EntityResponseType> {
    return this.http.post<IProductGroup>(this.resourceUrl, productGroup, { observe: 'response' });
  }

  update(productGroup: IProductGroup): Observable<EntityResponseType> {
    return this.http.put<IProductGroup>(this.resourceUrl, productGroup, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
