import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProductGroup } from 'app/shared/model/product-group.model';
import { AccountService } from 'app/core/auth/account.service';
import { ProductGroupService } from './product-group.service';

@Component({
  selector: 'jhi-product-group',
  templateUrl: './product-group.component.html'
})
export class ProductGroupComponent implements OnInit, OnDestroy {
  productGroups: IProductGroup[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected productGroupService: ProductGroupService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.productGroupService
      .query()
      .pipe(
        filter((res: HttpResponse<IProductGroup[]>) => res.ok),
        map((res: HttpResponse<IProductGroup[]>) => res.body)
      )
      .subscribe(
        (res: IProductGroup[]) => {
          this.productGroups = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInProductGroups();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IProductGroup) {
    return item.id;
  }

  registerChangeInProductGroups() {
    this.eventSubscriber = this.eventManager.subscribe('productGroupListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
