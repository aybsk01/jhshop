import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IProductGroup, ProductGroup } from 'app/shared/model/product-group.model';
import { ProductGroupService } from './product-group.service';

@Component({
  selector: 'jhi-product-group-update',
  templateUrl: './product-group-update.component.html'
})
export class ProductGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: []
  });

  constructor(protected productGroupService: ProductGroupService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ productGroup }) => {
      this.updateForm(productGroup);
    });
  }

  updateForm(productGroup: IProductGroup) {
    this.editForm.patchValue({
      id: productGroup.id,
      name: productGroup.name,
      description: productGroup.description
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const productGroup = this.createFromForm();
    if (productGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.productGroupService.update(productGroup));
    } else {
      this.subscribeToSaveResponse(this.productGroupService.create(productGroup));
    }
  }

  private createFromForm(): IProductGroup {
    return {
      ...new ProductGroup(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
