import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Jshop11SharedModule } from 'app/shared/shared.module';
import { ProductGroupComponent } from './product-group.component';
import { ProductGroupDetailComponent } from './product-group-detail.component';
import { ProductGroupUpdateComponent } from './product-group-update.component';
import { ProductGroupDeletePopupComponent, ProductGroupDeleteDialogComponent } from './product-group-delete-dialog.component';
import { productGroupRoute, productGroupPopupRoute } from './product-group.route';

const ENTITY_STATES = [...productGroupRoute, ...productGroupPopupRoute];

@NgModule({
  imports: [Jshop11SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ProductGroupComponent,
    ProductGroupDetailComponent,
    ProductGroupUpdateComponent,
    ProductGroupDeleteDialogComponent,
    ProductGroupDeletePopupComponent
  ],
  entryComponents: [ProductGroupDeleteDialogComponent]
})
export class Jshop11ProductGroupModule {}
