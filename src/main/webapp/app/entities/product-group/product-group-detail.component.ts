import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProductGroup } from 'app/shared/model/product-group.model';

@Component({
  selector: 'jhi-product-group-detail',
  templateUrl: './product-group-detail.component.html'
})
export class ProductGroupDetailComponent implements OnInit {
  productGroup: IProductGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ productGroup }) => {
      this.productGroup = productGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
