import { IProduct } from 'app/shared/model/product.model';

export interface IProductGroup {
  id?: number;
  name?: string;
  description?: string;
  products?: IProduct[];
}

export class ProductGroup implements IProductGroup {
  constructor(public id?: number, public name?: string, public description?: string, public products?: IProduct[]) {}
}
