import { IProductGroup } from 'app/shared/model/product-group.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string;
  price?: number;
  imageContentType?: string;
  image?: any;
  productGroup?: IProductGroup;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public price?: number,
    public imageContentType?: string,
    public image?: any,
    public productGroup?: IProductGroup
  ) {}
}
