import { IUser } from 'app/core/user/user.model';
import { IPurchase } from 'app/shared/model/purchase.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface IClient {
  id?: number;
  firstName?: string;
  lastName?: string;
  gender?: Gender;
  phone?: string;
  addressLine1?: string;
  user?: IUser;
  purchases?: IPurchase[];
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public gender?: Gender,
    public phone?: string,
    public addressLine1?: string,
    public user?: IUser,
    public purchases?: IPurchase[]
  ) {}
}
