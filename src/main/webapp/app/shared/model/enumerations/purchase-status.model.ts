export const enum PurchaseStatus {
  FINISHED = 'FINISHED',
  PENDING = 'PENDING'
}
