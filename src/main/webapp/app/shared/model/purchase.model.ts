import { Moment } from 'moment';
import { IPurchaseItem } from 'app/shared/model/purchase-item.model';
import { IClient } from 'app/shared/model/client.model';
import { PurchaseStatus } from 'app/shared/model/enumerations/purchase-status.model';

export interface IPurchase {
  id?: number;
  dt?: Moment;
  status?: PurchaseStatus;
  docNumber?: string;
  purchaseItems?: IPurchaseItem[];
  client?: IClient;
}

export class Purchase implements IPurchase {
  constructor(
    public id?: number,
    public dt?: Moment,
    public status?: PurchaseStatus,
    public docNumber?: string,
    public purchaseItems?: IPurchaseItem[],
    public client?: IClient
  ) {}
}
