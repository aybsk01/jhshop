import { IProduct } from 'app/shared/model/product.model';
import { IPurchase } from 'app/shared/model/purchase.model';

export interface IPurchaseItem {
  id?: number;
  quantity?: number;
  product?: IProduct;
  purchase?: IPurchase;
}

export class PurchaseItem implements IPurchaseItem {
  constructor(public id?: number, public quantity?: number, public product?: IProduct, public purchase?: IPurchase) {}
}
